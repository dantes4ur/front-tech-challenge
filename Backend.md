---
mainfont: "Work Sans"
author: "Spot^2"
geometry: margin=2cm
documentclass: extarticle
fontsize: 10pt
papersize: letter
header-includes:
    - \usepackage{graphicx}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \setlength\headheight{26pt}
    - \rhead{\includegraphics[width=2cm]{logo_green1x.png}}
    - \renewcommand{\headrulewidth}{0pt}
    - \renewcommand{\footrulewidth}{0pt}
    - \pagenumbering{gobble}
---

# Technical Challenge Spot

El reto consiste en generar un API de códigos postales tomando en cuenta los datos del siguiente [enlace](https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/Descarga.aspx).

Ejemplo:

**GET** /zip-code/01210

**Response:**
```json
{
   "status":true,
   "payload":[
      {
         "id":28130,
         "code":"01210",
         "settlement":"Santa Fe",
         "settlement_type":"Pueblo",
         "municipality":"\u00c1lvaro Obreg\u00f3n",
         "city":"Ciudad de M\u00e9xico",
         "zone":"Urbano",
         "state":{
            "id":7,
            "name":"Ciudad de M\u00e9xico"
         }
      }
   ]
}
```

## Resultados esperados

Puedes entregar en tu ambiente de desarrollo. Pero el API debe de funcionar y tu proyecto deberá contener todas las instrucciones para reproducirlo (migraciones, seeders, endpoints, etc).

> Bonus: Entregar también en un ambiente productivo.

## Limitaciones

- Tienes dos días para completar este tech challenge.
- Tech Stack: **Laravel** y SQL. Puedes realizarlo en otros stacks, pero deberás presentar porque lo desarrollaste así en la entrevista.

## Consejos

- Menos es más.
- Pensar fuera de la caja.
- Las migraciones y los seeders son herramientas muy útiles del proceso de desarrollo.
- ¿Pruebas unitarias? ¿Incluiste alguna?
- ¿Por qué deberíamos adoptar tu solución o qué mejorarías?
- ¿Este sería un código que pondrías en producción?

## Notes

- ¿Dudas? Envíame correo a dante@spot2.mx
