#  Technical challenge Spot² (Frontend)

You have to develop a mockup of our main application which is a map search engine. For this task, you have to create an account at our development environment [Staging](https://staging.spot2.mx). After this, you should send me an email so I can bootstrap your developer account. 

Our services use OAuth standards, so after I activate your account, you should test your setup to receive and refresh a token. 

This application is intented to check your knowledge and best practices developing React Applications and JS in general.

## Requirements 

1. Implement Map View as shown in the next section and search filters for variables like type, term and square_space (which is a number of square meters from the spot).

- *type*: This variable refers to the classfication in the platform and the possible values are:
    - 1: Street Spot
    - 2: In mall Spot
    - 3: In mall island
    - 4: Other 

- *term*:  This is used to check if the spot can be rented for these periods:
    - 1: Short Term
    - 2: Long Term
    - 3: Both

2. Setup your google maps / leaflet / mapbox canvas to show the spots from Mexico City. 

3. Consume REST API from staging.spot2.mx to retrieve the listed spots. The account you just created have the access permissions to send Bearer tokens to the protected endpoints. 

    - Remember to not plot the spots with the flag is_public set in 0. Or in case you plot them, show that is not available.

Example authentication (OAuth using JWT):

```bash
curl --request POST \
  --url https://staging.spot2.mx/api/auth/login \
  --header 'Content-Type: multipart/form-data' \
  --form email=candidate@spot2.mx \
  --form password=my-password-is-not-123456
```


```json
{
    "headers": {
        "Authorization": "Bearer X|YhrV99YWcDm2UOGYYNM6WDcbbwId8R2mfJf3B9BM"
    }
}
```

**Example:**

`GET /api/spots`
```JSON
{
  "status": true,
  "payload": {
    "current_page": 1,
    "data": [
      {
        "id": 1,
        "user_id": null,
        "company_id": 1,
        "name": "Polanco 232 Pie de calle",
        "square_space": "300.00",
        "description": null,
        "alias": null,
        "is_public": 0,
        "type": 1,
        "term": 3,
        "level": null,
        "zip_code_id": 29149,
        "city_id": 184,
        "street": "Av Horacio",
        "ext_number": "232",
        "int_number": null,
        "reference": "Esq. Horacio y Petrarca",
        "latitude": null,
        "longitude": null,
        "location": null,
        "created_at": "2021-08-17T22:50:30.000000Z",
        "updated_at": "2021-08-27T16:22:34.000000Z",
        "cache_favorites": 0,
        "people_flow": null,
        "people_flow_term": null,
        "uuid": "30cee49c-bb29-4233-ad03-3cb594c5989d",
        "zip_code": {
        "id": 29149,
        "code": "11550",
        "settlement": "Polanco IV Sección",
        "settlement_type": "Colonia",
        "municipality": "Miguel Hidalgo",
        "city": "Ciudad de México",
        "zone": "Urbano"
        }
      },
      ...
    ]
  }
}
```

4. If someone clicks on the pin (the spot), you should redirect them to a show view where you can display the spot information. 

`GET /api/spots/<spot_id>`

```json
{
  "status": true,
  "payload": {
    "id": 1,
    "user_id": null,
    "company_id": 1,
    "name": "Polanco 232 Pie de calle",
    "square_space": "300.00",
    "description": null,
    "alias": null,
    "is_public": 0,
    "type": 1,
    "term": 3,
    "level": null,
    "zip_code_id": 29149,
    "city_id": 184,
    "street": "Av Horacio",
    "ext_number": "232",
    "int_number": null,
    "reference": "Esq. Horacio y Petrarca",
    "latitude": null,
    "longitude": null,
    "location": null,
    "created_at": "2021-08-17T22:50:30.000000Z",
    "updated_at": "2021-08-27T16:22:34.000000Z",
    "cache_favorites": 0,
    "people_flow": null,
    "people_flow_term": null,
    "uuid": "30cee49c-bb29-4233-ad03-3cb594c5989d",
    "zip_code": {
      "id": 29149,
      "code": "11550",
      "settlement": "Polanco IV Sección",
      "settlement_type": "Colonia",
      "municipality": "Miguel Hidalgo",
      "city": "Ciudad de México",
      "zone": "Urbano"
    }
  }
}
```
--- 

- Examples: 
    - GET `https://staging.spot2.mx/api/spots`
    - GET `https://staging.spot2.mx/api/spots?sort=-created_at`
    - GET `https://staging.spot2.mx/api/spots?fields=name,type`
    - GET `https://staging.spot2.mx/api/spots/198`

Important: Do not forget to include Authorization Header

### Example interface

Here comes your creativity of showing the menus, filters and map as you want. The only constraint is the time you can spend developing this. 

![Mockup](./mockup.jpg)

## Expected result

A full working page where the query works and retrieves information from our development environment. The maps work correctly and the show view is displaying the correct information as well. 

## Constraints 

- You need to complete as much as you can in two days. Then we will have a meet where you can talk about yourself and how you completed this tech challenge.
- Use as less payload per request as you can. Query just the fields that you need. 

## Advices 

- Keep in mind to implement your solution as you would do it in the production code. Show us your best skills developing this prototype and why your code does cool stuff. 
- Think about what else you can bring to the project, how to improve? more features? how would you do it for mobile? Unit Testing, how would you implement this? 
- Think outside the box

## Notes

- Questionas about the challenge? You can contact me whenever you want dante@spot2.mx
